var express = require('express');
const db=require('../connection');
var router = express.Router();
const User = require("../models/user");
const Komentar = require("../models/komentar");
const Meni = require("../models/meni");
const Narocilo = require('../models/narocilo');
const Element = require('../models/element');
const Ocena = require('../models/ocena');
/* GET home page. */

router.use(express.static('views'));

router.get('/', function(req, res) {

  User.getRestaurants(0,(err, data) => {
    if (err)
      res.status(500).send({
        message: "Neuspesno branje podatkov"
      });
    else
    {
      res.render('home.html', {
        data: data
      });
    }
  });
});

router.post('/', function(req, res) {
  console.log("POST");
  var search=req.body.uporabniskoime;
  console.log(search);
  User.getRestaurant(0,search,(err, data) => {
    if (err)
    {
      res.status(500).send({
        message: "Neuspesno branje podatkov"
      });
    }
      
    else
    {
    console.log(data);
    if (data == null)
    {
      res.render('404.html');
    }
    else
    {
      
    
          res.render('home.html', {
            data: data
          });
      
    }
  }
});
});

router.get('/profil/:idUporabnik', function(req, res) {
  var idUporabnik = req.params.idUporabnik;
  User.getUser(idUporabnik,(err, data) => {
    if (err)
    {
      res.status(500).send({
        message: "Neuspesno branje podatkov"
      });
    }
    else
    {
    console.log(data);
      Komentar.get(idUporabnik,(err, komentari) => {
        if (err)
          res.status(500).send({
            message: "Neuspesno branje podatkov"
          });
        else
        {
          Meni.get(idUporabnik,(err, meni) => {
            if (err)
              res.status(500).send({
                message: "Neuspesno branje podatkov"
              });
            else
            {
              //
              Ocena.pov(idUporabnik,(err, ocena) => {
                if (err)
                  res.status(500).send({
                    message: "Neuspesno branje podatkov"
                  });
                else
                {
                  console.log(ocena);
              res.render('profil.html', {
                data: data,komentari:komentari,meni:meni,ocena:ocena
              });
            }
                });
                //
        }
            });
        }
      });

    }
  });
});



router.get('/narocila/:idNarocilo', function(req, res) {
  var idNarocilo = req.params.idNarocilo;
  var user=req.session.userid;

 console.log("alo");
  User.getUser(user,(err, data) => {
    if (err)
    {
      res.status(500).send({
        message: "Neuspesno branje podatkov"
      });
    }
    else
    {
      Element.get(idNarocilo,(err, el) => {
        if (err)
        {
          res.status(500).send({
            message: "Neuspesno branje podatkov"
          });
        }
        else
        {
          res.render('element.html', {
            data: data,el:el
          });
    //
        }
      });
//
    }
  });



 
});




router.get('/narocila', function(req, res) {
  var user=req.session.userid;

 
  User.getUser(user,(err, data) => {
    if (err)
    {
      res.status(500).send({
        message: "Neuspesno branje podatkov"
      });
    }
    else
    {

      User.check(user,(err, rest) => {
        if (err)
        {
          res.status(500).send({
            message: "Neuspesno branje podatkov"
          });
        }
        else
        {
    

        if (rest.length!=0)
        {
          
          Narocilo.getRestoran(user,(err, nar) => {
            if (err)
            {
              console.log("1");
              console.log(nar);
              res.status(500).send({
                message: "Neuspesno branje podatkov"
              });
            }
            else
            {
            
              res.render('narocila.html', {
                data: data,nar:nar
              });
            }
          });
        
        }

        else 
        {
//
Narocilo.get(user,(err, nar) => {
  
  if (err)
  {
    res.status(500).send({
      message: "Neuspesno branje podatkov"
    });
  }
  else
  {
    console.log("2");
    console.log(nar);
  
    res.render('narocila.html', {
      data: data,nar:nar
    });
  }
});

}

        }
       
        });
//
    }
  });



 
});

router.get('/login', function (req, res, next) {
  res.render('login.html');
});


router.post('/login', function (req, res, next) {

  var uporabnisko_ime = req.body.uporabnisko_ime;
  var geslo = req.body.password;
  let result;
  User.get(uporabnisko_ime, geslo, (err, data) => {
    if (data == null)
      res.redirect('/login');
    else {
      req.session.user = uporabnisko_ime;
      req.session.userid = data[0].idUporabnik;
      req.session.email = data[0].email;
      console.log(req.session.user);
      if (data[0].isAdmin == 1)
        res.redirect('/admin');
      else
        res.redirect('/');
    }
  })


});








router.post('/profil/:idUporabnik', function(req, res) {
  console.log("POST");
  var komentar=req.body.komentar;
  let nov_komentar = {
     
    vsebina : req.body.komentar,
    datum_obajve :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    Komentator : req.session.userid,
    Profil :req.params.idUporabnik
  };
  let nova_ocena={
    ocena : req.body.ocena,
    uporabnik : req.session.userid,
    profil : req.params.idUporabnik
};
  
  Komentar.dodaj(nov_komentar, (err, data) => {
    if (err)
      res.status(500).send({
        message: "Failed to add komentar"
      });
    else
    Ocena.dodaj(nova_ocena, (err, data) => {
      if (err)
        res.status(500).send({
          message: "Failed to add ocena"
        });
      else
      res.redirect('/profil/'+req.params.idUporabnik);

    })
  })
});

router.get('/logout', function (req, res, next) {
  req.session.destroy();
  res.redirect('/');
});
module.exports = router;
