var sql = require('../connection');
var Meni = function(meni){
    
    this.Naziv = meni.Naziv;
    this.Opis = meni.Opis;
    this.cena = meni.cena;
    this.Uporabnik_idUporabnik=meni.Uporabnik_idUporabnik;
};

Meni.dodaj = function (nov_meni, result) {    
        sql.query("INSERT INTO Meni set ?", nov_meni, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};

Meni.get = function (id,result) {    
    sql.query("SELECT * FROM Meni WHERE  Uporabnik_idUporabnik=? ",[id],  function (err, res, fields) {
                      
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        
        else{
            console.log(res);
            result(null,res);
        }
    });          
};

module.exports=Meni;