var sql = require('../connection');
var Ocena = function(ocena){
    
    this.ocena = ocena.ocena;
    this.uporabnik = ocena.uporabnik;
    this.profil = ocena.profil;
};

Ocena.dodaj = function (ocena, result) {    
        sql.query("INSERT INTO Ocena set ?", ocena, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};



Ocena.pov = function (id, result) {
    sql.query("SELECT sum/uporabnik as ocena FROM ( SELECT COUNT(ocena.uporabnik) as uporabnik, SUM(ocena) AS sum FROM ocena WHERE profil=? GROUP BY ocena.uporabnik) as inner_query", id, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res);
                result(null,res);
            }
        });           
};



module.exports=Ocena;