var sql = require('../connection');
var User = function(user){
    
    this.uporabniskoime = user.uporabniskoime;
    this.geslo = user.geslo;
    this.ime = user.ime;
    this.priimek = user.priimek;
    this.elekstronski_naslov = user.elekstronski_naslov;
    this.naslov = user.naslov;
    this.tel_stevilka = user.tel_stevilka;
    this.delovni_cas = user.delovni_cas;
    this.naziv = user.naziv;
    this.isAdmin = user.isAdmin;
    this.isRestaurant = user.isRestaurant;
};

User.dodaj = function (nov_uporabnik, result) {    
        sql.query("INSERT INTO Uporabnik set ?", nov_uporabnik, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};




User.get = function (uporabnisko_ime,geslo, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE uporabniskoime = ? && geslo = ?", [uporabnisko_ime, geslo], function (err, res) {
        if (res.length > 0 ) {
            console.log('Correct username and password');
            result(null, res);
        } else {
            console.log('Incorrect username and password');
            result(err, null);
        }			
        
    })

};



User.preveri = function (uporabnisko_ime,email, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE uporabniskoime = ? OR elekstronski_naslov = ?", [uporabnisko_ime,email ], function (err, res) {
        if (res.length <= 0 ) {
            console.log('Uporabnik ne obstaja');
            result(null, res);

        } else {
            console.log('Uporabnik obstaja');
            result(err, null);

        }			
        
    })

};



User.logIn = function (uporabnisko_ime,geslo, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE uporabniskoime = ? && geslo = ?", [uporabnisko_ime, geslo], function (err, res) {
        if (res.length > 0 ) {
            console.log('Login uspela');
            result(null, res);
        } else {
            console.log('Login ni uspela');
            result(err, null);
        }			
        
    })

};


User.check = function (id, result) {  
    sql.query("SELECT * FROM Uporabnik WHERE  uporabnik.isRestaurant= 1 AND uporabnik.idUporabnik=?", [id], function (err, res) {
         
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res);
            console.log("reeest");
            result(null,res);
        }
    });   

};



User.getRestaurants = function (id, result) {    
    sql.query("SELECT * FROM uporabnik WHERE uporabnik.isRestaurant= 1 AND uporabnik.idUporabnik!=?",[id],  function (err, res, fields) {
                      
        if (res.length > 0 ) {
            console.log('Restuarants found');
            result(null, res);
        } else {
            console.log('Error');
            result(err, null);
        }			
    })         
};


User.getRestaurant = function (id,search, result) {    
    sql.query("SELECT * FROM Uporabnik WHERE uporabnik.isRestaurant= 1 AND uporabnik.idUporabnik!=? AND uporabnik.naziv LIKE ?",[id,search],  function (err, res, fields) {
                      
        if (res.length > 0 ) {
            console.log('Uspesan prikaz uporabnika');
            result(null, res);
        } else {
            console.log('Ni uspesan prikaz ponudnika');
            result(err, null);
        }			
    })         
};

User.getUser = function (id,result) {    
    sql.query("SELECT * FROM Uporabnik WHERE  uporabnik.idUporabnik=? ",[id],  function (err, res, fields) {
                      
        if (res.length > 0 ) {
            console.log('Uspesan prikaz uporabnika');
            result(null, res);
        } else {
            console.log('Ni uspesan prikaz ponudnika');
            result(err, null);
        }			
    })         
};





module.exports=User;