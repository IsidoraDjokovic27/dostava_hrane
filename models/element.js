var sql = require('../connection');
var Element = function(element){
    
    this.kolicina = element.kolicina;
    this.cena = element.cena;
    this.Narocilo_idNarocilo = element.Narocilo_idNarocilo;
    this.Meni_idMeni = element.Meni_idMeni;

};

Element.dodaj = function (nov_element, result) {    
        sql.query("INSERT INTO Element set ?", nov_element, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};

Element.get = function (id, result) {    
    sql.query("SELECT *   FROM element,narocilo,meni WHERE element.Narocilo_idNarocilo=narocilo.idNarocilo AND element.Meni_idMeni=meni.idMeni AND Narocilo_idNarocilo=?", id, function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res);
                result(null,res);
            }
        });           
};

module.exports=Element;