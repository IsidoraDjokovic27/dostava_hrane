var sql = require('../connection');
var Narocilo = function(narocilo){
    
    this.cena = narocilo.cena;
    this.datum_narocila =  new Date().toISOString().slice(0, 19).replace('T', ' ');
    this.Narocilo_idUporabnik = narocilo.Narocilo_idUporabnik;
    this.Narocilo_kome = narocilo.Narocilo_kome;
    this.nacin_placila = narocilo.nacin_placila;

};

Narocilo.dodaj = function (novo_narocilo, result) {    
        sql.query("INSERT INTO Narocilo set ?", novo_narocilo, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};
Narocilo.getRestoran = function (id, result) {    
    sql.query(" SELECT * FROM Narocilo  LEFT JOIN uporabnik ON Narocilo.Narocilo_kome = uporabnik.idUporabnik  WHERE Narocilo_idUporabnik=? ORDER BY  datum_narocila DESC", [id], function (err, res) {
            
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log("getRest");
            console.log(res);
            result(null,res);
        }
    });           
};


Narocilo.get = function (id, result) {    
    sql.query(" SELECT * FROM Narocilo  LEFT JOIN uporabnik ON Narocilo.Narocilo_idUporabnik = uporabnik.idUporabnik  WHERE Narocilo_kome=? ORDER BY  datum_narocila DESC", [id], function (err, res) {
            
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log("get");
            console.log(res);
            result(null,res);
        }
    });           
};
module.exports=Narocilo;