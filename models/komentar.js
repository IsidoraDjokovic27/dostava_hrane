var sql = require('../connection');
var Komentar = function(komentar){
    
    this.vsebina = komentar.vsebina;
    this.datum_obajve =  new Date().toISOString().slice(0, 19).replace('T', ' ');
    this.Komentator = komentar.Komentator;
    this.Profil = komentar.Profil;

};

Komentar.dodaj = function (nov_komentar, result) {    
        sql.query("INSERT INTO Komentar set ?", nov_komentar, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res);
                    result(null,res);
                }
            });           
};

Komentar.broj = function (id, result) {    
    sql.query(" SELECT COUNT(idKomentar) AS broj FROM Komentar WHERE Profil=?", id, function (err, res) {
            
        if (res.length > 0 ) {
            console.log('Uspesan prikaz uporabnika');
            result(null, res);
        } else {
            console.log('Ni uspesan prikaz ponudnika');
            result(err, null);
        }			
    })         
};

Komentar.get = function (id, result) {    
    sql.query(" SELECT * FROM Komentar  LEFT JOIN uporabnik ON komentar.Komentator = uporabnik.idUporabnik  WHERE Profil=? ", [id,id], function (err, res) {
            
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res);
                result(null,res);
            }
        });           
};

module.exports=Komentar;