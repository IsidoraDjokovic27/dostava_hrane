const mysql=require('mysql');
const db=mysql.createConnection(
    {
      host:'localhost',
      user:'root',
      password:'password',
      database:'dostava_hrane',
      port:'3306'
    }
  )
  
  db.connect((err) => {
    if(err){
      console.log('Error connecting to Db'+err);
      return;
    }
    console.log('Connection established');
  });

  db.on('error', function(err) {
    console.log("[mysql error]",err);
  });
  
  
  module.exports=db;
