-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dostava_hrane
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dostava_hrane` ;

-- -----------------------------------------------------
-- Schema dostava_hrane
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dostava_hrane` DEFAULT CHARACTER SET utf8 ;
USE `dostava_hrane` ;

-- -----------------------------------------------------
-- Table `dostava_hrane`.`Uporabnik`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dostava_hrane`.`Uporabnik` ;

CREATE TABLE IF NOT EXISTS `dostava_hrane`.`Uporabnik` (
  `idUporabnik` INT NOT NULL AUTO_INCREMENT,
  `uporabniskoime` VARCHAR(20) NOT NULL,
  `geslo` VARCHAR(32) NOT NULL,
  `naslov` VARCHAR(45) NOT NULL,
  `tel_stevilka` VARCHAR(45) NOT NULL,
  `ime` VARCHAR(30) NOT NULL,
  `priimek` VARCHAR(30) NOT NULL,
  `elekstronski_naslov` VARCHAR(45) NOT NULL,
  `delovni_cas` TEXT NULL,
  `naziv` VARCHAR(35) NULL,
  `isAdmin` TINYINT(1) NULL DEFAULT 0,
  `isRestaurant` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idUporabnik`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dostava_hrane`.`Komentar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dostava_hrane`.`Komentar` ;

CREATE TABLE IF NOT EXISTS `dostava_hrane`.`Komentar` (
  `idKomentar` INT NOT NULL AUTO_INCREMENT,
  `vsebina` TEXT NOT NULL,
  `datum_obajve` DATE NOT NULL,
  `Komentator` INT NOT NULL,
  `Profil` INT NOT NULL,
  PRIMARY KEY (`idKomentar`, `Komentator`, `Profil`),
  INDEX `fk_Komentar_Uporabnik_idx` (`Komentator` ASC) VISIBLE,
  INDEX `fk_Komentar_Uporabnik1_idx` (`Profil` ASC) VISIBLE,
  CONSTRAINT `fk_Komentar_Uporabnik`
    FOREIGN KEY (`Komentator`)
    REFERENCES `dostava_hrane`.`Uporabnik` (`idUporabnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Komentar_Uporabnik1`
    FOREIGN KEY (`Profil`)
    REFERENCES `dostava_hrane`.`Uporabnik` (`idUporabnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dostava_hrane`.`Narocilo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dostava_hrane`.`Narocilo` ;

CREATE TABLE IF NOT EXISTS `dostava_hrane`.`Narocilo` (
  `idNarocilo` INT NOT NULL AUTO_INCREMENT,
  `datum_narocila` DATE NOT NULL,
  `cena` DOUBLE NOT NULL,
  `nacin_placila` TINYINT NOT NULL,
  `Narocilo_idUporabnik` INT NOT NULL,
  PRIMARY KEY (`idNarocilo`, `Narocilo_idUporabnik`),
  INDEX `fk_Narocilo_Narocilo1_idx` (`Narocilo_idUporabnik` ASC) VISIBLE,
  CONSTRAINT `fk_Narocilo_Narocilo1`
    FOREIGN KEY (`Narocilo_idUporabnik`)
    REFERENCES `dostava_hrane`.`Uporabnik` (`idUporabnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dostava_hrane`.`Meni`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dostava_hrane`.`Meni` ;

CREATE TABLE IF NOT EXISTS `dostava_hrane`.`Meni` (
  `idMeni` INT NOT NULL AUTO_INCREMENT,
  `Naziv` VARCHAR(30) NOT NULL,
  `Opis` TEXT NULL,
  `cena` DOUBLE NOT NULL,
  `Uporabnik_idUporabnik` INT NOT NULL,
  PRIMARY KEY (`idMeni`, `Uporabnik_idUporabnik`),
  INDEX `fk_Meni_Uporabnik1_idx` (`Uporabnik_idUporabnik` ASC) VISIBLE,
  CONSTRAINT `fk_Meni_Uporabnik1`
    FOREIGN KEY (`Uporabnik_idUporabnik`)
    REFERENCES `dostava_hrane`.`Uporabnik` (`idUporabnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dostava_hrane`.`Element`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dostava_hrane`.`Element` ;

CREATE TABLE IF NOT EXISTS `dostava_hrane`.`Element` (
  `idElement` INT NOT NULL AUTO_INCREMENT,
  `kolicina` INT NOT NULL,
  `cena` DOUBLE NOT NULL,
  `Narocilo_idNarocilo` INT NOT NULL,
  `Meni_idMeni` INT NOT NULL,
  PRIMARY KEY (`idElement`, `Narocilo_idNarocilo`, `Meni_idMeni`),
  INDEX `fk_Element_Narocilo1_idx` (`Narocilo_idNarocilo` ASC) VISIBLE,
  INDEX `fk_Element_Meni1_idx` (`Meni_idMeni` ASC) VISIBLE,
  CONSTRAINT `fk_Element_Narocilo1`
    FOREIGN KEY (`Narocilo_idNarocilo`)
    REFERENCES `dostava_hrane`.`Narocilo` (`idNarocilo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Element_Meni1`
    FOREIGN KEY (`Meni_idMeni`)
    REFERENCES `dostava_hrane`.`Meni` (`idMeni`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
